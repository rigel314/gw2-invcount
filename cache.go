package main

import (
	"compress/gzip"
	"encoding/gob"
	"log"
	"os"
)

// loadCache loads cached gw2 items and currency api outputs.  This prevents the code from having to query the same IDs over and over.
func loadCache() (ic itemCache, cc currencyCache) {
	// defer an error message, which can be prevented by setting fail=true before return
	fail := true
	defer func() {
		if fail {
			log.Println("problem with gw2 api cache, ignoring cache")
		}
	}()

	ic = make(itemCache)     // this return value needs to be valid, even if there are errors
	cc = make(currencyCache) // this return value needs to be valid, even if there are errors
	var ver string

	// open cache file for reading
	f, err := os.Open("cache")
	if err != nil {
		return
	}
	defer f.Close()

	// cache file should be gzipped, so prepare to read gzipped data, and unzip it on the fly
	grd, err := gzip.NewReader(f)
	if err != nil {
		return
	}
	defer grd.Close()

	// cache file shuold be gob-encoded, so prepare to read gob data, and deserialize on the fly
	dec := gob.NewDecoder(grd)

	// decode version string
	err = dec.Decode(&ver)
	if err != nil && ver != getVersion() {
		return make(itemCache), make(currencyCache)
	}

	// decode item cache
	err = dec.Decode(&ic)
	if err != nil {
		return make(itemCache), make(currencyCache)
	}

	// decode currency cache
	err = dec.Decode(&cc)
	if err != nil {
		return ic, make(currencyCache)
	}

	// if we're here, we didn't error, so prevent the deferred error message from printing
	fail = false

	return
}

// saveCache saves cached gw2 item and currency api outputs.  This prevents the code from having to query the same IDs over and over.
func saveCache(inames itemCache, cnames currencyCache) {
	// defer an error message, which can be prevented by setting fail=true before return
	fail := true
	defer func() {
		if fail {
			log.Println("problem writing cache")
		}
	}()

	// create cache file
	f, err := os.Create("cache")
	if err != nil {
		return
	}
	defer f.Close()

	// cache file is gzipped to save space, so prepare to compress on the fly
	gwr := gzip.NewWriter(f)
	defer gwr.Close()

	// cache file is gob-encoded for ease of serializing, so prepare to serialize on the fly
	enc := gob.NewEncoder(gwr)

	// encode version
	err = enc.Encode(getVersion())
	if err != nil {
		return
	}

	// encode item cache
	err = enc.Encode(inames)
	if err != nil {
		return
	}

	// encode currency cache
	err = enc.Encode(cnames)
	if err != nil {
		return
	}

	fail = false
}
