module gitlab.com/rigel314/gw2-invcount

go 1.19

require gitlab.com/MrGunflame/gw2api v1.0.3

require (
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.2 // indirect
)

replace gitlab.com/MrGunflame/gw2api => gitlab.com/rigel314/gw2api v1.0.4-0.20230606014831-683ebfb91f38

// replace gitlab.com/MrGunflame/gw2api => ../gw2api
