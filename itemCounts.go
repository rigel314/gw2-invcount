package main

// charCounts holds metadata for everywhere a character is holding an item
type charCounts struct {
	Bags           int
	InventoryCount int
	EquipedCount   int
}

// itemCount holds metadata for everywhere an account has an item stored
type itemCount struct {
	Total                int
	Characters           map[string]charCounts
	SharedInventoryCount int
	MaterialStorageCount int
	BankCount            int
	LegendaryArmoryCount int
	CurrencyCount        int
	GuildStashCount      map[string]int
}

type itemCounts map[int]itemCount // item ID to count lookup

type currencyCounts map[int]int // currency ID to count lookup

// makeItem initializes an itemCount and stores it in items
func (items itemCounts) makeItem(id int) itemCount {
	v, ok := items[id]
	if !ok {
		v = itemCount{
			Characters:      make(map[string]charCounts),
			GuildStashCount: make(map[string]int),
		}
		items[id] = v
	}
	return v
}

// makeChar adds a character-specific charCounts to the corresponding itemCount entry of items
func (items itemCounts) makeChar(id int, n string) itemCount {
	ic := items.makeItem(id)
	_, ok := ic.Characters[n]
	if !ok {
		ic.Characters[n] = charCounts{}
	}
	return ic
}

// makeGuild adds a guild-specific stash count to the corresponding itemCount entry of items
func (items itemCounts) makeGuild(id int, n string) itemCount {
	ic := items.makeItem(id)
	_, ok := ic.GuildStashCount[n]
	if !ok {
		ic.GuildStashCount[n] = 0
	}
	return ic
}

// CharacterBagAdd adds the given item as a character-specific equipped bag
func (items itemCounts) CharacterBagAdd(id int, count int, n string) {
	ic := items.makeChar(id, n)
	ic.Total += count
	counts := ic.Characters[n]
	counts.Bags += count
	ic.Characters[n] = counts
	items[id] = ic
}

// CharacterInventoryAdd adds the given item as a character-specific held-in-inventory item
func (items itemCounts) CharacterInventoryAdd(id int, count int, n string) {
	ic := items.makeChar(id, n)
	ic.Total += count
	counts := ic.Characters[n]
	counts.InventoryCount += count
	ic.Characters[n] = counts
	items[id] = ic
}

// CharacterEquipedAdd adds the given item as a character-specific equipped item
func (items itemCounts) CharacterEquipedAdd(id int, count int, n string) {
	ic := items.makeChar(id, n)
	ic.Total += count
	counts := ic.Characters[n]
	counts.EquipedCount += count
	ic.Characters[n] = counts
	items[id] = ic
}

// SharedInventoryAdd adds the given item as an item stored in shared inventory
func (items itemCounts) SharedInventoryAdd(id int, count int) {
	ic := items.makeItem(id)
	ic.Total += count
	ic.SharedInventoryCount += count
	items[id] = ic
}

// MaterialStorageAdd adds the given item as an item stored in material storage
func (items itemCounts) MaterialStorageAdd(id int, count int) {
	ic := items.makeItem(id)
	ic.Total += count
	ic.MaterialStorageCount += count
	items[id] = ic
}

// BankAdd adds the given item as an item stored in the bank
func (items itemCounts) BankAdd(id int, count int) {
	ic := items.makeItem(id)
	ic.Total += count
	ic.BankCount += count
	items[id] = ic
}

// GuildAdd adds the given item as a guild-specific item in a guild stash
func (items itemCounts) GuildAdd(id int, count int, n string) {
	ic := items.makeGuild(id, n)
	ic.Total += count
	ic.GuildStashCount[n] += count
	items[id] = ic
}

// LegendaryArmorySet add the given item as an item stored in the legendary armory
// This assigns the total, rather than adding to the total, since a single legendary can be equipped by multiple characters
// This does not overwrite the metadata indicating which characeters are equipping the legendaries
func (items itemCounts) LegendaryArmorySet(id int, count int) {
	ic := items.makeItem(id)
	ic.Total = count // if these are equiped on multiple charecters, each character would increment the count, this overrides
	ic.LegendaryArmoryCount = count
	items[id] = ic
}
