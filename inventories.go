package main

import (
	"log"
	"strings"

	"gitlab.com/MrGunflame/gw2api"
)

// characterInv looks up all the account's character's bags, inventory, and equipment
func characterInv(api *gw2api.Session, items itemCounts) {
	log.Println("checking characters")

	// get a list of all characters
	chars, err := api.Characters()
	if err != nil {
		panic(err)
	}
	for _, char := range chars {
		log.Println("  " + char)

		// get a list of the bags for this character
		bags, err := api.CharacterInventory(char)
		if err != nil {
			panic(err)
		}
		for _, bag := range bags {
			if bag == nil {
				continue
			}

			// save this bag's ID as an item
			items.CharacterBagAdd(bag.ID, 1, char)

			// loop through all items in this bag
			for _, item := range bag.Inventory {
				if item == nil {
					continue
				}

				// save this item ID
				items.CharacterInventoryAdd(item.ID, item.Count, char)
			}
		}

		// get a list of all the equipment for this character
		equipment, err := api.CharacterEquipment(char)
		if err != nil {
			panic(err)
		}
		for _, equip := range equipment {
			if equip == nil {
				continue
			}

			// save this item ID
			items.CharacterEquipedAdd(equip.ID, 1, char)
		}
	}
}

// sharedInv looks up all the items stored in shared inventory slots
func sharedInv(api *gw2api.Session, items itemCounts) {
	log.Println("checking shared inventory")

	// get a list of all the items in shared inventory slots
	is, err := api.AccountSharedInventory()
	if err != nil {
		panic(err)
	}
	for _, i := range is {
		if i == nil {
			continue
		}

		// save this item ID
		items.SharedInventoryAdd(i.ID, i.Count)
	}
}

// materialStorageInv looks up all the items stored in account material storage
func materialStorageInv(api *gw2api.Session, items itemCounts) {
	log.Println("checking material storage")

	// get a list of all items in material storage
	ms, err := api.AccountMaterials()
	if err != nil {
		panic(err)
	}
	for _, m := range ms {
		if m == nil {
			continue
		}
		if m.Count == 0 {
			continue
		}

		// save this item ID
		items.MaterialStorageAdd(m.ID, m.Count)
	}
}

// bankInv looks up all the items stored in the account bank
func bankInv(api *gw2api.Session, items itemCounts) {
	log.Println("checking bank")

	// get a list of all items in the bank
	bk, err := api.AccountBank()
	if err != nil {
		panic(err)
	}
	for _, bi := range bk {
		if bi == nil {
			continue
		}

		// save this item ID
		items.BankAdd(bi.ID, bi.Count)
	}
}

// guildInv looks up all the items stored in the guild stash of any specified guilds
func guildInv(api *gw2api.Session, items itemCounts, g string) {
	// no guilds specified, return
	if g == "" {
		return
	}

	log.Println("checking guild stashes")

	// get account guilds
	acc, err := api.Account()
	if err != nil {
		panic(err)
	}

	// save a lookup table for guild name to Guild struct
	m := make(map[string]gw2api.Guild)
	for _, g := range acc.Guilds {
		guild, err := api.Guild(g, true)
		if err != nil {
			panic(err)
		}
		m[guild.Name] = guild
	}

	// get specified guilds
	guilds := strings.Split(g, ",")

	for _, g := range guilds {
		// complain if account is not a member of specified guild, but continue
		if _, ok := m[g]; !ok {
			log.Printf("account is not a member of %q (%v)\n", g, m)
			continue
		}

		// get a list of the guild stash containers for this guild
		gscs, err := api.GuildStash(m[g].ID)
		if err != nil {
			panic(err)
		}
		for _, gsc := range gscs {
			if gsc == nil {
				continue
			}

			// loop through all the items in this guild slot container
			for _, gs := range gsc.Inventory {
				if gs == nil {
					continue
				}

				// save this item ID
				items.GuildAdd(gs.ID, gs.Count, g)
			}
		}
	}
}

// legendaryArmoryInv looks up all the bound legendary items for the account.
// This should be called after any character inventories are looked up, since
// each character can have a single account legendary equipped on each
// equipment tab.  This function assigns the total count, rather than adding
// to the total count.
func legendaryArmoryInv(api *gw2api.Session, items itemCounts) {
	log.Println("checking legendary armory")

	// get a list of all bound legendary items
	la, err := api.AccountLegendaryArmory()
	if err != nil {
		panic(err)
	}
	for _, li := range la {
		if li == nil {
			continue
		}

		// save this item ID
		items.LegendaryArmorySet(li.ID, li.Count)
	}
}

// walletInv looks up all the amounts of currencies owned by the account
func walletInv(api *gw2api.Session, currencies currencyCounts) {
	log.Println("checking wallet")

	// get a list of all owned currencies
	curs, err := api.AccountWallet()
	if err != nil {
		panic(err)
	}
	for _, cur := range curs {
		if cur == nil {
			continue
		}

		// save this currency ID
		currencies[cur.ID] = cur.Value
	}
}
