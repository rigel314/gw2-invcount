package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"text/template"

	"gitlab.com/MrGunflame/gw2api"
)

var (
	api        = flag.String("api", "", "api key")
	iid        = flag.Int("iid", -1, "item ID")
	cid        = flag.Int("cid", -1, "currency ID")
	srt        = flag.String("sort", "name", "name|count|id")
	csv        = flag.Bool("csv", false, "output CSV")
	html       = flag.Bool("html", false, "output html")
	ofile      = flag.String("o", "", "output filename, when -html is passed, this will also create the name with '.json' appended")
	guildstash = flag.String("guildstashes", "", "comma seperated guild names to include stashes")
)

type itemCache map[int]gw2api.Item         // item ID to item name lookup
type currencyCache map[int]gw2api.Currency // currency ID to currency name lookup

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile) // show line number in log messages
	flag.Parse()                                 // parse command line args

	// always print version
	log.Println(getVersion())

	// verify arguments
	if *api == "" {
		log.Fatal("must pass -api")
	}
	if *ofile == "" && *iid != -1 && *cid != -1 {
		log.Fatal("must pass -ofile when asking for all items; or pass [-iid id|-cid id] to ask for only one item")
	}

	// parse the html template when -html is specified
	var t *template.Template
	if *html {
		t = template.Must(template.New("html").Parse(htmlTemplate))
	}

	// create an object for accessing the api, with our api token
	api := gw2api.New().WithAccessToken(*api)

	// create a map for holding information about each item ID
	items := make(itemCounts)

	// find all the items currently owned by the account associated with the api token
	characterInv(api, items)
	sharedInv(api, items)
	materialStorageInv(api, items)
	bankInv(api, items)
	guildInv(api, items, *guildstash)
	legendaryArmoryInv(api, items) // must be called after characterInv() to fix claiming too many legendaries, since each legendary can be equiped by all compatible characters

	// create a map for holding information about each currency ID
	currencies := make(currencyCounts)
	// find all the currencies owned by the account associated with the api token
	walletInv(api, currencies)

	// if -iid was specified, just print out the count and name of that item ID
	if *iid != -1 {
		i, err := api.Items(*iid)
		if err != nil {
			panic(err)
		}
		if i[0] == nil {
			panic("no such item: " + fmt.Sprint(i))
		}
		// if it's in the map, we have some non-zero count, otherwise the account has none
		if c, ok := items[*iid]; ok {
			fmt.Println(c, i[0].Name)
		} else {
			fmt.Println(0, i[0].Name)
		}
		return
	}

	// if -cid was specified, just print out the count and name of that currency ID
	if *cid != -1 {
		i, err := api.Currencies(*cid)
		if err != nil {
			panic(err)
		}
		if i[0] == nil {
			panic("no such currency: " + fmt.Sprint(i))
		}
		// if it's in the map, we have some non-zero count, otherwise the account has none
		if c, ok := currencies[*cid]; ok {
			fmt.Println(c, i[0].Name)
		} else {
			fmt.Println(0, i[0].Name)
		}
		return
	}

	// print out some stats
	log.Println("found", len(items), "items in inventories")
	log.Println("found", len(currencies), "currencies")

	// make a sorted list of all the item IDs
	iids := make([]int, 0, len(items))
	for k := range items {
		iids = append(iids, k)
	}
	sort.Ints(iids)

	// make a sorted list of all the currency IDs
	cids := make([]int, 0, len(currencies))
	for k := range currencies {
		cids = append(cids, k)
	}
	sort.Ints(cids)

	// load any locally cached info, so we don't have to hit the API as hard
	itemNames, currencyNames := loadCache()

	// filter both lists of ids to find only the ones not in the cache
	lookupItemIds := filterIDs(iids, itemNames)
	lookupCurrIds := filterIDs(cids, currencyNames)

	dirty := false

	// lookup any missing item IDs
	if len(lookupItemIds) > 0 {
		dirty = true

		is, err := getitems(api, lookupItemIds)
		if err != nil {
			panic(err)
		}
		for _, v := range is {
			if v == nil {
				continue
			}
			itemNames[v.ID] = *v
		}
	}

	// lookup any missing currency IDs
	if len(lookupCurrIds) > 0 {
		dirty = true

		log.Println("looking up", len(lookupCurrIds), "items", lookupCurrIds)
		cs, err := api.Currencies(lookupCurrIds...)
		if err != nil {
			panic(err)
		}
		for _, v := range cs {
			if v == nil {
				continue
			}
			currencyNames[v.ID] = *v
		}
	}

	// overwrite the cache with new information from lookups, if anything new was looked up
	if dirty {
		saveCache(itemNames, currencyNames)
	}

	// info about invalid IDs is not returned by the api, despite the inventory api returning these IDs, so complain and put a placeholder in the map
	lookupItemIds = filterIDs(lookupItemIds, itemNames)
	for _, i := range lookupItemIds {
		log.Printf("invalid item ID: %d, %+v\n", i, items[i])
		itemNames[i] = gw2api.Item{
			Name: "Invalid Item ID",
			Icon: "https://render.guildwars2.com/file/FA1D042B0845BED8DA3CFA0FAA0837D5EB0207A6/61297.png",
		}
	}
	// info about invalid IDs is not returned by the api, I haven't seen this happen with currencies, so complain and put a placeholder in the map
	lookupCurrIds = filterIDs(lookupCurrIds, currencyNames)
	for _, i := range lookupCurrIds {
		log.Printf("invalid currency ID: %d, %+v\n", i, currencies[i])
		currencyNames[i] = gw2api.Currency{
			Name: "Invalid Currency ID",
			Icon: "https://render.guildwars2.com/file/FA1D042B0845BED8DA3CFA0FAA0837D5EB0207A6/61297.png",
		}
	}

	// make a list of output objects
	objs := make([]*object, 0, len(items)+len(currencies))
	// fill in the list for all the items
	for id, v := range items {
		objs = append(objs, &object{
			Name:      itemNames[id].Name,
			Icon:      itemNames[id].Icon,
			ID:        id,
			Locations: v,
			Type:      "item",
			Rarity:    raritySanitize(itemNames[id].Rarity),
		})
	}
	// fill in the list for all the currencies
	for id, v := range currencies {
		objs = append(objs, &object{
			Name: currencyNames[id].Name,
			Icon: currencyNames[id].Icon,
			ID:   id,
			Locations: itemCount{
				Total:           v,
				CurrencyCount:   v,
				Characters:      make(map[string]charCounts),
				GuildStashCount: make(map[string]int),
			},
			Type: "currency",
		})
	}

	// sort the output objects array using the -sort argument to decide how to sort
	switch *srt {
	case "name": // default argument value
		// sort by name, then ID
		sort.Slice(objs, func(i, j int) bool {
			if objs[i].Name == objs[j].Name {
				return objs[i].ID < objs[j].ID
			}
			return objs[i].Name < objs[j].Name
		})
	case "count":
		// sort by total count, then ID
		sort.Slice(objs, func(i, j int) bool {
			if objs[i].Locations.Total == objs[j].Locations.Total {
				return objs[i].ID < objs[j].ID
			}
			return objs[i].Locations.Total < objs[j].Locations.Total
		})
	default: // also handling case "id"
		// just sort by ID
		sort.Slice(objs, func(i, j int) bool {
			return objs[i].ID < objs[j].ID
		})
	}

	// create output file, overwriting if necessary
	f, err := os.Create(*ofile)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// print a header if exporting to csv
	if *csv {
		fmt.Fprintf(f, "count,name,id,locations\n")
	}
	// declare an htmltmpl object in case html is used
	var htmlobj htmltmpl
	// loop through all the output objects
	for _, v := range objs {
		switch true {
		case *csv:
			// print a line of CSV for each object
			fmt.Fprintf(f, "%d,%q,%d,%q\n", v.Locations.Total, v.Name, v.ID, fmt.Sprintf("%+v", v.Locations))
		case *html:
			// add each output object to the htmltmple object
			v.Hash = v.Type + "." + fmt.Sprint(v.ID)
			htmlobj.Objects = append(htmlobj.Objects, v)
		default:
			// print a line of human readable text for each object
			fmt.Fprintf(f, "% 6d %s [%d] %s\n", v.Locations.Total, v.Name, v.ID, fmt.Sprintf("%+v", v.Locations))
		}
	}

	if *html {
		htmlobj.Version = getVersion()
		htmlobj.Timestamp = getTimestamp()
		// run the html template with the htmltmpl object to generate the output file
		// this will write an html file like www/items.html with the "{{ }}" sections replaced with values from the htmltmpl object
		err := t.Execute(f, htmlobj)
		if err != nil {
			panic(err)
		}

		// create json output file
		f2, err := os.Create(*ofile + ".json")
		if err != nil {
			panic(err)
		}
		defer f2.Close()

		// encode the whole htmltmpl object into the json file
		jenc := json.NewEncoder(f2)
		jenc.SetIndent("", "\t")
		jenc.Encode(htmlobj)
	}
}

// raritySanitize passes strings that are known item rarities, and replaces any other string with "InvalidRarity"
func raritySanitize(r string) string {
	switch r {
	case "Junk", "Basic", "Fine", "Masterwork", "Rare", "Exotic", "Ascended", "Legendary":
		return r
	}

	return "InvalidRarity"
}

// getitems wraps gw2api.Items and calls it multiple times with <= 128 ids per call, since the gw2 api has a limit on the total number of item IDs that can be requested.
func getitems(api *gw2api.Session, ids []int) ([]*gw2api.Item, error) {
	log.Println("looking up", len(ids), "items", ids)
	is := make([]*gw2api.Item, 0, len(ids))
	// until the ids array is empty
	for len(ids) > 0 {
		// limit slice to 128 elements, or end of array, whichever is shorter
		end := len(ids)
		if end > 128 {
			end = 128
		}
		// actually call the api
		itms, err := api.Items(ids[:end]...)
		if err != nil {
			if _, ok := err.(*gw2api.Error); !ok {
				return nil, err
			}
			// allow this specific error, since apparently the inventory api can return item IDs which the items api thinks are invalid
			if err.(*gw2api.Error).Text != "all ids provided are invalid" {
				return nil, err
			}
		}
		is = append(is, itms...)
		// reslice ids to contain only the part we haven't used yet
		ids = ids[end:]
	}

	return is, nil
}

// filterIDs returns a list of ids which are not keys of cache
// This is a generic function because it only needs to look at map keys, T could have been `any`, but the rest of the code only uses it on Item or Currency
func filterIDs[T gw2api.Item | gw2api.Currency](ids []int, cache map[int]T) (idsfilt []int) {
	for _, v := range ids {
		if _, ok := cache[v]; !ok {
			idsfilt = append(idsfilt, v)
		}
	}

	return
}
