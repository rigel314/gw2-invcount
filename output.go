package main

import (
	_ "embed"
	"runtime/debug"
	"time"
)

// object is an output object, this contains metadata for each item/currency
type object struct {
	Hash      string
	Name      string
	Icon      string
	ID        int
	Locations itemCount
	Type      string
	Rarity    string
}

//go:embed www/items.html
var htmlTemplate string // htmlTemplate is used to generate html with some values filled in

// htmltmpl is the object passed to template.Execute for outputting an html page
type htmltmpl struct {
	Version   string
	Timestamp int64
	Objects   []*object
}

// getVersion returns the git hash of the source used to build the executable or "Uncontrolled"
func getVersion() string {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		return "Uncontrolled"
	}

	for _, v := range info.Settings {
		if v.Key == "vcs.revision" {
			return v.Value
		}
	}
	return "Uncontrolled"
}

// getTimestamp returns the current unix timestamp in seconds
func getTimestamp() int64 {
	return time.Now().Unix()
}
